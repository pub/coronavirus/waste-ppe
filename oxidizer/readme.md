Initial evaluation of oxidizer method for sterilization

This topic can be divided into two main categories: [methods vaporizing a liquid](https://www.cdc.gov/infectioncontrol/guidelines/disinfection/sterilization/other-methods.html#anchor_1554397455), e.g. H2O2, then heating the mixture into a plasma, and [plasmification of gas directly](https://www.cdc.gov/infectioncontrol/guidelines/disinfection/sterilization/other-methods.html#anchor_1554397475). A system that has inputs only consisting of air and electricity would be the ideal scenario as it means it would function without the challenges of a liquid supply chain.

Before proceeding down a path to build any sterilization system there needs to be a biological or virological indicator that can be used as a control. Geobacillus stearothermophilus spores (Botelho-Almeida 2008) were used in a few different studies due to their stericidal resistance. 3m has a [flourescent tagged version](https://multimedia.3m.com/mws/media/497747O/attest-rrbi-for-steam-1292-package-insert-english.pdf) which may prove useful for this purpose.

to be continued...


prior art:
[Apparatus And Method For Using Ozone As A Disinfectant](https://patents.google.com/patent/US20120020830A1/en)
[Development of a Practical Method for Using Ozone Gas as a Virus Decontaminating Agent](https://www.tandfonline.com/doi/full/10.1080/01919510902747969?src=recsys)
[Evaluating the Potential, Applicability, and Effectiveness of Ozone Sterilization Process for Medical Devices](https://link.springer.com/article/10.1007/s12247-017-9308-7)

